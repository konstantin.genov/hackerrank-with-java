package com.kgenov.ProblemSolving._17_MigratoryBirds;

import java.io.*;
import java.util.*;

public class Solution {

    //input is array of ints corresponding to bird type
    //find all the sightings of each bird by int id, then find which type/s is most common
    //if the number of sightings between types is two or more, print only the one with the smallest id
    static int migratoryBirds(List<Integer> arr) {
        HashMap<Integer, Integer> sightings = new HashMap<>();
        for (Integer birdId : arr
        ) {
            if (sightings.containsKey(birdId)) {
                sightings.merge(birdId, 1, Integer::sum);
            } else {
                sightings.put(birdId, 1);
            }
        }
        int mostSightings = sightings.entrySet().stream().mapToInt(b -> b.getValue()).max().getAsInt();

        return sightings.entrySet().stream().filter(b -> b.getValue() == mostSightings).mapToInt(b -> b.getKey()).min().getAsInt();
    }

    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

        int arrCount = Integer.parseInt(bufferedReader.readLine().trim());

        String[] arrItems = bufferedReader.readLine().replaceAll("\\s+$", "").split(" ");

        List<Integer> arr = new ArrayList<>();

        for (int i = 0; i < arrCount; i++) {
            int arrItem = Integer.parseInt(arrItems[i]);
            arr.add(arrItem);
        }

        int result = migratoryBirds(arr);
        bufferedReader.close();
        System.out.println(result);
    }
}

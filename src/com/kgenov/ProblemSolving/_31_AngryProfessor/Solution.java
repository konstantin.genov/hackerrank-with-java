package com.kgenov.ProblemSolving._31_AngryProfessor;
/* konstantin created on 12/4/2020 inside the package - com.kgenov.ProblemSolving._31_AngryProfessor */

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner;

public class Solution {

    // a math. professor has a class of students. frustrated with their lack of discipline, the professor decides to cancel class
    // if fewer than some number of students are present when class starts.
    // input: int k - threshold of number of students
    // int[] a - the arrival times of the students
    // arrivalTime go from on time -> arrivalTime <= 0 to arrived late arrivalTime > 0
    static String angryProfessor(int k, int[] a) {
        int arriveOnTime = 0;
        int studentsOnTime = (int) Arrays.stream(a).filter(student -> student <= arriveOnTime).count();

        return studentsOnTime >= k ? "NO" : "YES";
    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {

        int t = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        for (int tItr = 0; tItr < t; tItr++) {
            String[] nk = scanner.nextLine().split(" ");

            int n = Integer.parseInt(nk[0]);

            int k = Integer.parseInt(nk[1]);

            int[] a = new int[n];

            String[] aItems = scanner.nextLine().split(" ");
            scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

            for (int i = 0; i < n; i++) {
                int aItem = Integer.parseInt(aItems[i]);
                a[i] = aItem;
            }

            String result = angryProfessor(k, a);

            System.out.println(result);
        }

        scanner.close();
    }
}
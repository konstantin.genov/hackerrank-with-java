package com.kgenov.ProblemSolving._20_SalesByMatch;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Solution {

    // problem: large pile of socks for sale, they need to be grouped by color; find out how many pairs of socks with matching colors there are
    // input:
    // n - number of socks in the pile
    // ar[] - colors of each sock represented by integer value
    static int sockMerchant(int n, int[] ar) {
        HashMap<Integer, Integer> coloredSocks = new HashMap<>();

        Arrays.stream(ar).forEach(sock -> {
            if (coloredSocks.containsKey(sock)) coloredSocks.merge(sock, 1, Integer::sum);
            else coloredSocks.put(sock, 1);
        });

        return coloredSocks.entrySet()
                .stream()
                .filter(socks -> socks.getValue() >= 2)
                .mapToInt(sockPair -> sockPair.getValue() % 2 == 0 ? sockPair.getValue() / 2 : (sockPair.getValue() - (sockPair.getValue() % 2)) / 2)
                .sum();

    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {

        int n = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        int[] ar = new int[n];

        String[] arItems = scanner.nextLine().split(" ");
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        for (int i = 0; i < n; i++) {
            int arItem = Integer.parseInt(arItems[i]);
            ar[i] = arItem;
        }

        int result = sockMerchant(n, ar);

        scanner.close();
        System.out.println(result);
    }
}

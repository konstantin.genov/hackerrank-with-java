package com.kgenov.ProblemSolving._10_GradingStudents;
/* konstantin created on 7/28/2020 inside the package - com.kgenov.ProblemSolving.GradingStudents */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

class Result {

    /*
     * Complete the 'gradingStudents' function below.
     *
     * The function is expected to return an INTEGER_ARRAY.
     * The function accepts INTEGER_ARRAY grades as parameter.
     */

    public static List<Integer> gradingStudents(List<Integer> grades) {
        List<Integer> finalGrades = new ArrayList<>();


        for (Integer grade : grades
        ) {
            double multipleCalculation = Math.ceil(Double.valueOf(grade)/ 5) * 5;
            System.out.println("Grade " + grade + " Multiple : " + multipleCalculation);
            if (multipleCalculation - grade < 3 && multipleCalculation >= 40) {
                finalGrades.add((int) multipleCalculation);
            } else {
                finalGrades.add(grade);
            }
        }
        return finalGrades;
    }

}

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

        int gradesCount = Integer.parseInt(bufferedReader.readLine().trim());

        List<Integer> grades = IntStream.range(0, gradesCount).mapToObj(i -> {
            try {
                return bufferedReader.readLine().replaceAll("\\s+$", "");
            } catch (IOException ex) {
                throw new RuntimeException(ex);
            }
        })
                .map(String::trim)
                .map(Integer::parseInt)
                .collect(Collectors.toList());

        List<Integer> result = Result.gradingStudents(grades);

        System.out.println(result);
        bufferedReader.close();
    }
}

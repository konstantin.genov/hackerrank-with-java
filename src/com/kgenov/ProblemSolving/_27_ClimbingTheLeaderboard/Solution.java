package com.kgenov.ProblemSolving._27_ClimbingTheLeaderboard;

import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.*;
import java.util.regex.*;
import java.util.stream.*;

import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;

class Result {

    /*
     * The function is expected to return an INTEGER_ARRAY.
     * The function accepts following parameters:
     *  1. INTEGER_ARRAY ranked
     *  2. INTEGER_ARRAY player
     */

    // The player with the highest score is ranked 1st. Players who have equal scores receive the same ranking.
    // Next players receive the immediately following ranking number -> Jane 100 points (1st), John 90 points (2nd),
    // George 90 points (2nd), Peter 85 points (3rd);
    // We have to go through score iterations for a player, and for each new score return their ranking number
    // Ranked array - current highscores; player array - scores for each game (iterate and return as mentioned above)
    public static List<Integer> climbingLeaderboard(List<Integer> ranked, List<Integer> player) {
        // sort current highscores

        List<Integer> playerScores = new ArrayList<>();
        // array with the distinct scores allows us to easily calculate the ranking position as it respond 1:1 to the index
        // meaning the highest score is 100 located at index 0, we always calculate index + 1 to get the ranking position
        int[] intStream = ranked.stream().distinct().sorted().mapToInt(i -> i).toArray();

        // after we add Alice's score to the current scores, this is our new score array size
        int totalIndexes = intStream.length + 1;

        for (int roundScore : player
        ) {

            // refer to documentation to understand arrays.binarySearch approach for index calculation
            int j = Arrays.binarySearch(intStream, roundScore);

            // if binarySearch returns a negative, means index needs to be inserted from the end of the array to the start
            if (j < 0) {

                // if the length of the list + the index of the binary search is equal to the totalIndexes of scores, it means we are at last ranking
                if (intStream.length + Math.abs(j) == totalIndexes) {
                    playerScores.add(totalIndexes);

                } else { // adjust index calculation -> j returns insert position - 1, so we re-add the 1 to find the index
                    int tempCalculation = (totalIndexes - Math.abs(j)) + 1;
                    playerScores.add(tempCalculation);
                }
            } else { // when positive remove insertion index from the maximum allowed indexes, then remove -1 to avoid duplicate records
                int tempCalculation = (totalIndexes - j) - 1;
                playerScores.add(tempCalculation);

            }
        }

        return playerScores;
    }

}


public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

        int rankedCount = Integer.parseInt(bufferedReader.readLine().trim());

        List<Integer> ranked = Stream.of(bufferedReader.readLine().replaceAll("\\s+$", "").split(" "))
                .map(Integer::parseInt)
                .collect(toList());

        int playerCount = Integer.parseInt(bufferedReader.readLine().trim());

        List<Integer> player = Stream.of(bufferedReader.readLine().replaceAll("\\s+$", "").split(" "))
                .map(Integer::parseInt)
                .collect(toList());

        List<Integer> result = Result.climbingLeaderboard(ranked, player);
        bufferedReader.close();
        System.out.println(result);
    }
}

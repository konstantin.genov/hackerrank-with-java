package com.kgenov.ProblemSolving._03_DiagonalDifference;
/* konstantin created on 7/27/2020 inside the package - com.kgenov.ProblemSolving.DiagonalDifference */

import java.io.*;
import java.util.*;

class Result {

    /*
     * Complete the 'diagonalDifference' function below.
     *
     * The function is expected to return an INTEGER.
     * The function accepts 2D_INTEGER_ARRAY arr as parameter.
     */

    public static int diagonalDifference(List<List<Integer>> arr) {
        // Write your code here
        int leftToRightDiagonal = 0, rightToLeftDiagonal = 0;
        for (int i = 0; i < arr.size(); i++) {
            leftToRightDiagonal += arr.get(i).get(i);
            rightToLeftDiagonal += arr.get(arr.size()-i-1).get(i);
        }

        return Math.abs(leftToRightDiagonal - rightToLeftDiagonal);

    }

}

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

        int n = Integer.parseInt(bufferedReader.readLine().trim());

        List<List<Integer>> arr = new ArrayList<>();

        for (int i = 0; i < n; i++) {
            String[] arrRowTempItems = bufferedReader.readLine().replaceAll("\\s+$", "").split(" ");

            List<Integer> arrRowItems = new ArrayList<>();

            for (int j = 0; j < n; j++) {
                int arrItem = Integer.parseInt(arrRowTempItems[j]);
                arrRowItems.add(arrItem);
            }

            arr.add(arrRowItems);
        }

        int result = Result.diagonalDifference(arr);
        System.out.println(result);

        bufferedReader.close();
    }
}

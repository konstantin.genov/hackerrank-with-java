package com.kgenov.ProblemSolving._29_DesignerPDF_Viewer;

import java.io.IOException;
import java.util.HashMap;
import java.util.Scanner;

public class Solution {

    // When a contiguous block of text is selected in a PDF viewer, the selection is highlighted with a blue rectangle.
    // In this PDF viewer, each word is highlighted independently
    // there is a list of 26 character heights aligned by index to their letters, meaning we will receive an int array with corresponding height
    // values for each letter -> eg: 1, 3, 1, 4 -> a[1], b[3], c[3], d[4];
    // we will also receive a string: a word for which we have to find it's tallest letter (char with highest corresponding int value)
    // we have to determine the area of rectangle highlight in mm^2, assuming all letters are 1mm wide; meaning -> tallest letter * word length = area
    static int designerPdfViewer(int[] h, String word) {
        HashMap<Character, Integer> charNumPairs = new HashMap<>();
        int tallestLetter = 0; // var used in finding the tallest letter
        // variable to iterate h array containing int values for each index/letter pair
        int index = 0;
        for (char ch = 'a'; ch <= 'z'; ch++) {
            charNumPairs.put(ch, h[index]);
            index++;
        }
        // map each char in the word to it's corresponding int value and find the tallest
        for (int i = 0; i < word.length(); i++) {
            int letterHeight = charNumPairs.get(word.charAt(i));
            if (letterHeight > tallestLetter) tallestLetter = letterHeight;
        }

        return tallestLetter * word.length();
    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {

        int[] h = new int[26];

        String[] hItems = scanner.nextLine().split(" ");
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        for (int i = 0; i < 26; i++) {
            int hItem = Integer.parseInt(hItems[i]);
            h[i] = hItem;
        }

        String word = scanner.nextLine();

        int result = designerPdfViewer(h, word);

        System.out.println(result);

        scanner.close();
    }
}

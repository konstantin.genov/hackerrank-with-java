package com.kgenov.ProblemSolving._30_UtopianTree;
/* konstantin created on 12/3/2020 inside the package - com.kgenov.ProblemSolving._30_UtopianTree */


import java.io.IOException;
import java.util.Scanner;

public class Solution {

    // The Utopian Tree goes through 2 cycles of growth every year.
// Each spring, it doubles in height. Each summer, its height increases by 1 meter.
// A Utopian Tree sapling with a height of 1 meter is planted at the onset of spring. How tall will the tree be after 'n' growth cycles?
// return the height of the tree each growth cycle
    static int utopianTree(int n) {
        int height = 1; // tree starts with this height
        int springHeightGrow = 2; // doubles, multiply
        int summerHeightGrow = 1; // increases, addition

        if (n == 0) { // no height change
            return height;
        }

        for (int i = 0; i < n; ) {
            // spring calculation - 1 cycle
            height = height * springHeightGrow;
            i++;
            if (i == n) {
                return height;
            }
            // summer calculation - 1 cycle
            height = height + summerHeightGrow;
            i++;
        }
        return height;
    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {

        int t = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        for (int tItr = 0; tItr < t; tItr++) {
            int n = scanner.nextInt();
            scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

            int result = utopianTree(n);
            System.out.println("Height: " + result);
        }


        scanner.close();
    }
}

package com.kgenov.ProblemSolving._28_TheHurdleRace;

import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;

public class Solution {

    // a character competes in a hurdle race, hurdles are of varying heights, and the characters have a maximum height they can jump.
    // there is a magic potion they can take which increases their jump height by 1 for each dose.
    // how many doses must the character take to be able to jump all hurdles - return the number
    // if the character can already jump the hurdles, return 0
    // input:
    // int k -  maximum height character can jump
    // int[] height - arrays of the heights character should jump over
    static int hurdleRace(int k, int[] height) {
        // get biggest height from the array
        int biggestHeight = Arrays.stream(height).max().getAsInt();

        // character can already jump through all heights
        if (k >= biggestHeight) {
            return 0;
        }

        return biggestHeight - k;
    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {

        String[] nk = scanner.nextLine().split(" ");

        int n = Integer.parseInt(nk[0]);

        int k = Integer.parseInt(nk[1]);

        int[] height = new int[n];

        String[] heightItems = scanner.nextLine().split(" ");
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        for (int i = 0; i < n; i++) {
            int heightItem = Integer.parseInt(heightItems[i]);
            height[i] = heightItem;
        }

        int result = hurdleRace(k, height);
        scanner.close();

        System.out.println(result);
    }
}

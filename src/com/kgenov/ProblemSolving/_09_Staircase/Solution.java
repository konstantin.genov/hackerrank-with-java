package com.kgenov.ProblemSolving._09_Staircase;
/* konstantin created on 7/27/2020 inside the package - com.kgenov.ProblemSolving.Staircase */

import java.util.Scanner;

public class Solution {

    // Complete the staircase function below.
    static void staircase(int n) {
        for (int i = 0; i < n; i++) {
            String emptyLine = " ";
            String symbol = "#";

            for (int j = n-i-1; j > 0; j--) {
                System.out.print(emptyLine);
            }

            for (int j = 0; j <= i; j++) {
                System.out.print(symbol);
            }
            System.out.println();
        }
    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int n = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        staircase(n);

        scanner.close();
    }
}


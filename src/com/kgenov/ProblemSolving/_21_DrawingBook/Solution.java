package com.kgenov.ProblemSolving._21_DrawingBook;

import java.io.*;
import java.math.*;
import java.text.*;
import java.util.*;
import java.util.regex.*;

public class Solution {

    // teacher asks the class to open their books to a page number, a student can start turning pages from the front or the back of the book
    // students always turn 1 page at a time, page 1 is on the right side; when they flip the first page, they see pages 2 and 3
    // each page except the last will be printed on both sides
    // input:
    // n - length of the book
    // p - page students need to turn to
    static int pageCount(int n, int p) {
        double pageTurns = Math.floor(p / 2); // when we turn once we see 2 pages
        double totalTurns = Math.floor(n / 2);

        return (int) Math.min(pageTurns, totalTurns - pageTurns);
    }


    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int p = scanner.nextInt();
        int result = pageCount(n, p);
        System.out.println(result);
        scanner.close();
    }
}

package com.kgenov.ProblemSolving._08_TimeConversion;
/* konstantin created on 7/28/2020 inside the package - com.kgenov.ProblemSolving.TimeConversion */

import java.io.IOException;
import java.util.Scanner;

public class Solution {

    static String timeConversion(String s) {
        boolean startsWith12 = s.startsWith("12");

        if (s.contains("PM")) {
            if (startsWith12) {
                return s.substring(0, 8);
            }
            int temp = Integer.parseInt(s.substring(0, 8).replaceAll(":", "")) + 120000;
            String formatTime = String.valueOf(temp).replaceAll("(.{2})", "$1:");
            return formatTime.substring(0, formatTime.length() - 1);
        } else {
            if (startsWith12) {
                return s.replaceAll("12", "00").substring(0, 8);
            }
            return s.substring(0, 8);
        }

    }

    private static final Scanner scan = new Scanner(System.in);

    public static void main(String[] args) throws IOException {

        String s = scan.nextLine();

        String result = timeConversion(s);
        System.out.println(result);
    }
}

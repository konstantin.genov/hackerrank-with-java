package com.kgenov.ProblemSolving._18_DayOfTheProgrammer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Solution {
    //Marie invented a Time Machine and wants to test it by time-traveling to visit Russia on the Day of the Programmer (the 256th day of the year)
    //during a year in the inclusive range from 1700 to 2700.
    //From 1700 to 1917, Russia's official calendar was the Julian calendar; since 1919 they used the Gregorian calendar system. The transition from the Julian to Gregorian calendar system occurred in 1918, when the next day after January 31st was February 14th.
    //This means that in 1918, February 14th was the 32nd day of the year in Russia.

    // if input year is 1918 -> return 26.09
    // check for leap years - divisible by 400, or divisible by 4 and not divisible by 100
    // sample input: 2017 -> should produce output: 13.09.2017
    static String dayOfProgrammer(int year) {

        if (year == 1918) return "26.09.1918";
        if (isLeap(year)) return "12.09." + year;
        else return "13.09." + year;

    }

    static boolean isLeap(int year) {

        if (year % 4 != 0) return false;
        if (year > 1918 && year % 100 == 0 && year % 400 != 0) return false;
        return true;

    }

    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

        int year = Integer.parseInt(bufferedReader.readLine().trim());

        String result = dayOfProgrammer(year);
        bufferedReader.close();
        System.out.println(result);
    }
}

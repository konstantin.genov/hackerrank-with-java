package com.kgenov.ProblemSolving._06_PlusMinus;
/* konstantin created on 7/27/2020 inside the package - com.kgenov.ProblemSolving.PlusMinus */

import java.text.DecimalFormat;
import java.util.*;

public class Solution {

    // Complete the plusMinus function below.
    static void plusMinus(int[] arr) {
        double positiveRatio = 0, negativeRatio = 0, zeroRatio = 0;
        int n = arr.length;

        for (int i = 0; i < n; i++) {
            double comparisonResult = Math.signum((double) arr[i]);

            if (comparisonResult > 0) {
                positiveRatio += 1;
            } else if (comparisonResult < 0) {
                negativeRatio += 1;
            } else {
                zeroRatio += 1;
            }
        }

        positiveRatio = positiveRatio / n;
        negativeRatio = negativeRatio / n;
        zeroRatio = zeroRatio / n;

        DecimalFormat df = new DecimalFormat("#.#####");

        System.out.println(df.format(positiveRatio));
        System.out.println(df.format(negativeRatio));
        System.out.println(df.format(zeroRatio));

    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int n = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        int[] arr = new int[n];

        String[] arrItems = scanner.nextLine().split(" ");
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        for (int i = 0; i < n; i++) {
            int arrItem = Integer.parseInt(arrItems[i]);
            arr[i] = arrItem;
        }

        plusMinus(arr);

        scanner.close();
    }
}

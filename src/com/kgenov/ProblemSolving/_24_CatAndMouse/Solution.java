package com.kgenov.ProblemSolving._24_CatAndMouse;
/* konstantin created on 11/18/2020 inside the package - com.kgenov.ProblemSolving._24_CatAndMouse */

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class Solution {

    // Two cats and a mouse are at various positions on a line, you will be given their starting positions.
    // Your task is to determine which cat will reach the mouse first, assuming mouse does not move and cats travel at the same speed.
    // if the cats arrive at the same time, the mouse will be allowed to move and escape

    /**
     * @param x - integer, position of cat A
     * @param y - integer, position of cat B
     * @param z - mouse's position
     * @return
     */
    static String catAndMouse(int x, int y, int z) {
        int catA = Math.abs(x - z);
        int catB = Math.abs(y - z);

        if (catA < catB) {
            return "Cat A";
        } else if (catB < catA) {
            return "Cat B";
        } else {
            return "Mouse C";
        }
    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {

        int q = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r])?");

        for (int qItr = 0; qItr < q; qItr++) {
            String[] xyz = scanner.nextLine().split(" ");

            int x = Integer.parseInt(xyz[0]);

            int y = Integer.parseInt(xyz[1]);

            int z = Integer.parseInt(xyz[2]);

            String result = catAndMouse(x, y, z);
            System.out.println(result);
        }

        scanner.close();
    }
}
package com.kgenov.ProblemSolving._11_AppleAndOrange;
/* konstantin created on 7/28/2020 inside the package - com.kgenov.ProblemSolving.AppleAndOrange */

import java.util.Scanner;

public class Solution {

    static void countApplesAndOranges(int s, int t, int a, int b, int[] apples, int[] oranges) {
        // s is start of the house, t is end of the house; both combined represent the size of the house
        // a - location of apple tree; b - location of orange tree
        // first array - apples
        // second array - oranges
        int applesOnHouse = 0;
        int orangesOnHouse = 0;

        for (int i = 0; i < apples.length; i++) {
            int appleLandingPosition = apples[i];
            if (appleLandingPosition + a >= s
                    && appleLandingPosition + a <= t) {
                applesOnHouse++;
            }
        }

        for (int i = 0; i < oranges.length; i++) {
            int orangeLandingPosition = oranges[i];
            if (b + orangeLandingPosition >= s && b + orangeLandingPosition <= t){
                orangesOnHouse++;
            }
        }

        System.out.println(applesOnHouse);
        System.out.println(orangesOnHouse);

    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        String[] st = scanner.nextLine().split(" ");

        int s = Integer.parseInt(st[0]);

        int t = Integer.parseInt(st[1]);

        String[] ab = scanner.nextLine().split(" ");

        int a = Integer.parseInt(ab[0]);

        int b = Integer.parseInt(ab[1]);

        String[] mn = scanner.nextLine().split(" ");

        int m = Integer.parseInt(mn[0]);

        int n = Integer.parseInt(mn[1]);

        int[] apples = new int[m];

        String[] applesItems = scanner.nextLine().split(" ");
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        for (int i = 0; i < m; i++) {
            int applesItem = Integer.parseInt(applesItems[i]);
            apples[i] = applesItem;
        }

        int[] oranges = new int[n];

        String[] orangesItems = scanner.nextLine().split(" ");
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        for (int i = 0; i < n; i++) {
            int orangesItem = Integer.parseInt(orangesItems[i]);
            oranges[i] = orangesItem;
        }

        countApplesAndOranges(s, t, a, b, apples, oranges);

        scanner.close();
    }
}

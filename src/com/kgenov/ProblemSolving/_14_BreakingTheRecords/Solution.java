package com.kgenov.ProblemSolving._14_BreakingTheRecords;
/* konstantin created on 11/5/2020 inside the package - com.kgenov.ProblemSolving._14_BreakingTheRecords */

import java.util.Arrays;
import java.util.Scanner;

public class Solution {

    // Complete the breakingRecords function below.
    static int[] breakingRecords(int[] scores) {
        int lowestRecord = scores[0], highestRecord = scores[0];
        int lowestBroken = 0, highestBroken = 0;

        for (int game : scores
        ) {
            if (lowestRecord > game) {
                lowestRecord = game;
                lowestBroken++;
            } else if (highestRecord < game) {
                highestRecord = game;
                highestBroken++;
            }
        }
        return new int[]{highestBroken, lowestBroken};

    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {

        int n = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        int[] scores = new int[n];

        String[] scoresItems = scanner.nextLine().split(" ");
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        for (int i = 0; i < n; i++) {
            int scoresItem = Integer.parseInt(scoresItems[i]);
            scores[i] = scoresItem;
        }

        int[] result = breakingRecords(scores);
        System.out.println(Arrays.toString(result));
        scanner.close();
    }
}

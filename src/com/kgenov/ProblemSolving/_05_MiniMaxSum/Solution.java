package com.kgenov.ProblemSolving._05_MiniMaxSum;
/* konstantin created on 7/27/2020 inside the package - com.kgenov.ProblemSolving.MiniMaxSum */

import java.util.Scanner;

public class Solution {

    // Complete the miniMaxSum function below.
    static void miniMaxSum(int[] arr) {
        long largestNumber = arr[0];
        long smallestNumber = Long.MAX_VALUE;
        long sum = 0;

        for (int i = 0; i < arr.length; i++) {
            if (arr[i] < smallestNumber) {
                smallestNumber = arr[i];
                System.out.println("Smallest number in for: " + smallestNumber);
            } else if (arr[i] > largestNumber) {
                largestNumber = arr[i];
                System.out.println("Largest number in for: " + largestNumber);
            }
            sum += arr[i];
            System.out.println(sum);
        }
        long smallSum = sum - largestNumber;
        long bigSum = sum - smallestNumber;
        System.out.print(smallSum + " " + bigSum);
    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int[] arr = new int[5];

        String[] arrItems = scanner.nextLine().split(" ");
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        for (int i = 0; i < 5; i++) {
            int arrItem = Integer.parseInt(arrItems[i]);
            arr[i] = arrItem;
        }

        miniMaxSum(arr);

        scanner.close();
    }
}
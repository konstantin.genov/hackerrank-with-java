package com.kgenov.ProblemSolving._07_BirthdayCakeCandles;
/* konstantin created on 7/28/2020 inside the package - com.kgenov.ProblemSolving.BirthdayCakeCandles */

import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner;

public class Solution {

    // Complete the birthdayCakeCandles function below.
    static int birthdayCakeCandles(int[] ar) {
        int tallestCandle = Integer.MIN_VALUE;

        for (int value : ar) {
            if (tallestCandle < value) {
                tallestCandle = value;
            }
        }

        int finalTallestCandle = tallestCandle;
        int[] blownOutCandles = Arrays.stream(ar).filter(p -> p == finalTallestCandle).toArray();

        return blownOutCandles.length;

    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {

        int arCount = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        int[] ar = new int[arCount];

        String[] arItems = scanner.nextLine().split(" ");
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        for (int i = 0; i < arCount; i++) {
            int arItem = Integer.parseInt(arItems[i]);
            ar[i] = arItem;
        }

        int result = birthdayCakeCandles(ar);
        System.out.println(result);


        scanner.close();
    }
}

package com.kgenov.ProblemSolving._12_NumberLineJumps;
/* konstantin created on 11/5/2020 inside the package - com.kgenov.ProblemSolving._12_NumberLineJumps */

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class Solution {

    // Complete the kangaroo function below.
    static String kangaroo(int x1, int v1, int x2, int v2) {
        String response = "NO";
        boolean canCatchUp = (v2 < v1);
        if (canCatchUp) {
            boolean willIntersectOnLand = (x1 - x2) % (v2 - v1) == 0;
            if (willIntersectOnLand) {
                response = "YES";
            }
        }
        return response;

    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {

        String[] x1V1X2V2 = scanner.nextLine().split(" ");

        int x1 = Integer.parseInt(x1V1X2V2[0]);

        int v1 = Integer.parseInt(x1V1X2V2[1]);

        int x2 = Integer.parseInt(x1V1X2V2[2]);

        int v2 = Integer.parseInt(x1V1X2V2[3]);

        String result = kangaroo(x1, v1, x2, v2);

        scanner.close();
        System.out.println(result);
    }
}

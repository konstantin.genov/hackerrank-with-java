package com.kgenov.ProblemSolving._15_SubArrayDivision;

import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.*;
import java.util.regex.*;
import java.util.stream.*;

import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;

public class Solution {

    // Complete the birthday function below.
    static int birthday(List<Integer> s, int d, int m) {
        int birthDay = d, birthMonth = m, barSegments = 0;
        int[] chocolateBars = s.stream().mapToInt(i -> i).toArray();
        // array size minus the birth month equals the amount of times we need to traverse the chocolate bar pieces
        // E.g. input:
        //        5 - array size
        //        1 2 1 3 2 - numerical values in array,
        //        3(birthday) 2(birthmonth)
        // with this input we'd need to do 4 traversals to check 2 elements if combined they match the birthday

        for (int i = 0; i <= chocolateBars.length - birthMonth; i++) {
            if (Arrays.stream(chocolateBars, i, i + birthMonth).sum() == birthDay) { // turn array into sub arrays of length == birthMonth
                barSegments++;
            }
        }
        return barSegments;
    }

    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

        int n = Integer.parseInt(bufferedReader.readLine().trim());

        List<Integer> s = Stream.of(bufferedReader.readLine().replaceAll("\\s+$", "").split(" "))
                .map(Integer::parseInt)
                .collect(toList());

        String[] dm = bufferedReader.readLine().replaceAll("\\s+$", "").split(" ");

        int d = Integer.parseInt(dm[0]);

        int m = Integer.parseInt(dm[1]);

        int result = birthday(s, d, m);

        bufferedReader.close();
        System.out.println(result);
    }
}

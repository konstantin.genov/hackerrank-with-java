package com.kgenov.ProblemSolving._04_CompareTheTriplets;
/* konstantin created on 7/27/2020 inside the package - com.kgenov.ProblemSolving.CompareTheTriplets */

import java.io.*;
import java.util.*;

public class Solution {

    // Complete the compareTriplets function below.
    static List<Integer> compareTriplets(List<Integer> a, List<Integer> b) {
        List<Integer> pointsAwarded = Arrays.asList(0, 0);
        int pointsForAlice = 1, pointsForBob = 1;
        int aliceIndex = 0, bobIndex = 1;

        for (int i = 0; i < a.size(); i++) {
            if (a.get(i) > b.get(i)) pointsAwarded.set(aliceIndex, pointsForAlice++);
            if (a.get(i) < b.get(i)) pointsAwarded.set(bobIndex, pointsForBob++);
        }
        return pointsAwarded;
    }

    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

        String[] aItems = bufferedReader.readLine().replaceAll("\\s+$", "").split(" ");

        List<Integer> a = new ArrayList<>();

        for (int i = 0; i < 3; i++) {
            int aItem = Integer.parseInt(aItems[i]);
            a.add(aItem);
        }

        String[] bItems = bufferedReader.readLine().replaceAll("\\s+$", "").split(" ");

        List<Integer> b = new ArrayList<>();

        for (int i = 0; i < 3; i++) {
            int bItem = Integer.parseInt(bItems[i]);
            b.add(bItem);
        }

        List<Integer> result = compareTriplets(a, b);
        result.forEach(System.out::println);


        bufferedReader.close();
    }
}
package com.kgenov.ProblemSolving._13_BetweenTwoSets;
/* konstantin created on 11/5/2020 inside the package - com.kgenov.ProblemSolving._13_BetweenTwoSets */

/*
1. Find LCM of the first array a. 2.Find GCD / HCF of the second array b. 3.Find all the multiples of LCM up to GCD, which divides the GCD evenly.

For Example: Here, In the given sample Input, The LCM of array a would be 4 and the GCD of the array b would be 16. Now, Find all Multiples of 4,(like 4,8,12,16,...) upto 16,
 such that, (16%multiple_of_4_here) should be 0. Here, 16%4=0 -----> count=1 (suppose this variable.) 16%8=0 -----> count=2 16%12!=0 ---> count=2 16%16=0 ---> count=3.
Thus, The answer is 3
 */

import java.io.*;
import java.util.List;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

class Result {

    public static int getTotalX(List<Integer> a, List<Integer> b) {
        int count = 0;
        int[] firstArray = a.stream().mapToInt(i -> i).toArray();
        int lcmFirstArray = findArrayLCM(firstArray, 0, firstArray.length);

        int[] secondArray = b.stream().mapToInt(i -> i).toArray();
        int gcdSecondArray = findArrayGCD(secondArray);
        System.out.println("LCM of first array: " + lcmFirstArray);
        System.out.println("GCD of second array: " + gcdSecondArray);

        // now find all multiples of LCM up to GCD

        int multiplicationFactor = 1;
        int lcmMultiple = lcmFirstArray * multiplicationFactor;
        while (gcdSecondArray >= lcmMultiple) {
            if (gcdSecondArray % lcmMultiple == 0) {
                count++;
            }
            multiplicationFactor++;
            lcmMultiple = lcmFirstArray * multiplicationFactor;

        }
        return count;
    }


    //for array a
    public static int findArrayLCM(int[] arr, int start, int end) {
        if ((end - start) == 1) return lcm(arr[start], arr[end - 1]);
        else return (lcm(arr[start], findArrayLCM(arr, start + 1, end)));
    }

    //for array b
    static int findArrayGCD(int[] arr) {
        int result = arr[0];
        for (int i = 1; i < arr.length; i++) {
            result = gcd(arr[i], result);
        }
        return result;
    }

    public static int gcd(int a, int b) {
        while (b > 0) {
            int temp = b;
            b = a % b; // % is remainder
            a = temp;
        }
        return a;
    }

    public static int lcm(int a, int b) {
        return ((a * b) / gcd(a, b));

    }
}

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

        String[] firstMultipleInput = bufferedReader.readLine().replaceAll("\\s+$", "").split(" ");

        int n = Integer.parseInt(firstMultipleInput[0]);

        int m = Integer.parseInt(firstMultipleInput[1]);

        List<Integer> arr = Stream.of(bufferedReader.readLine().replaceAll("\\s+$", "").split(" "))
                .map(Integer::parseInt)
                .collect(toList());

        List<Integer> brr = Stream.of(bufferedReader.readLine().replaceAll("\\s+$", "").split(" "))
                .map(Integer::parseInt)
                .collect(toList());

        int total = Result.getTotalX(arr, brr);
        bufferedReader.close();
        System.out.println(total);
    }
}

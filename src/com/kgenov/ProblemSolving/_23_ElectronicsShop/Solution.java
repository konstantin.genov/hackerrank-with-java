package com.kgenov.ProblemSolving._23_ElectronicsShop;
/* konstantin created on 11/9/2020 inside the package - com.kgenov.ProblemSolving._23_ElectronicsShop */

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;

public class Solution {

    //a person wants to determine the most expensive keyboard and USB drive that can be purchased with a given budget
    // given price lists for both items, find the cost to buy them; if it's not possible, return -1
    // input: keyboards arr - value of each keyboard, same with drives, int b - budget
    static int getMoneySpent(int[] keyboards, int[] drives, int b) {
        ArrayList<Integer> combinations = new ArrayList<Integer>();
        for (int keyboard : keyboards) {
            for (int drive : drives) {
                if (keyboard + drive <= b) { //get all combinations which match our budget restriction
                    combinations.add(keyboard + drive);
                }
            }
        }
        // if there are elements in the AL and the biggest value is smaller or equal than the budget return it, otherwise return -1
        return !combinations.isEmpty() && Collections.max(combinations) <= b ? Collections.max(combinations) : -1;
    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {

        String[] bnm = scanner.nextLine().split(" ");
        int b = Integer.parseInt(bnm[0]);

        int n = Integer.parseInt(bnm[1]);

        int m = Integer.parseInt(bnm[2]);

        int[] keyboards = new int[n];

        String[] keyboardsItems = scanner.nextLine().split(" ");

        for (int keyboardsItr = 0; keyboardsItr < n; keyboardsItr++) {
            int keyboardsItem = Integer.parseInt(keyboardsItems[keyboardsItr]);
            keyboards[keyboardsItr] = keyboardsItem;
        }

        int[] drives = new int[m];

        String[] drivesItems = scanner.nextLine().split(" ");

        for (int drivesItr = 0; drivesItr < m; drivesItr++) {
            int drivesItem = Integer.parseInt(drivesItems[drivesItr]);
            drives[drivesItr] = drivesItem;
        }

        int moneySpent = getMoneySpent(keyboards, drives, b);

        scanner.close();

        System.out.println(moneySpent);
    }
}

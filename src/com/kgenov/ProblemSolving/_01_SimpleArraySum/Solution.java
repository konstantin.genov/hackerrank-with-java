package com.kgenov.ProblemSolving._01_SimpleArraySum;
/* konstantin created on 7/27/2020 inside the package - com.kgenov.ProblemSolving.SimpleArraySum */

import java.io.*;
import java.util.*;

public class Solution {

    /*
     * Complete the simpleArraySum function below.
     */
    static int simpleArraySum(int[] ar) {
       int arraySum = 0;
        for (int i = 0; i < ar.length; i++) {
            arraySum += ar[i];
        }
        return arraySum;

    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {

        int arCount = Integer.parseInt(scanner.nextLine().trim());

        int[] ar = new int[arCount];

        String[] arItems = scanner.nextLine().split(" ");

        for (int arItr = 0; arItr < arCount; arItr++) {
            int arItem = Integer.parseInt(arItems[arItr].trim());
            ar[arItr] = arItem;
        }

        int result = simpleArraySum(ar);

        System.out.println(result);
    }
}

package com.kgenov.ProblemSolving._25_FillingJars;
/* konstantin created on 11/24/2020 inside the package - com.kgenov.ProblemSolving._25_FillingJars */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

public class Solution {

    // N empty jars, numbered from 1 to N
    // performs M operations - each operation is described by 3 ints:
    // int a, int b are indexes of the jars, int k is the number of candies to be added inside
    // each jar whose index lies between a and b(both inclusive)
    // Return the average number of candies after M operations
    static int solve(int n, int m, int[] a, int[] b, int[] k) {
        int totalCandies = 0;
        for (int i = 0; i < m; i++) {
            // +1 to indices of jars, as they include the indexes passed -> index 1 and 2 -> 2 jars, not 1 jar
            // indices 2 and 5 -> 2, 3, 4, 5 indexes
            int jarsIteration = Math.abs(b[i] - a[i] + 1);
            totalCandies =  totalCandies + (k[i] * jarsIteration);
        }
        return totalCandies / n;
    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

        String[] nm = scanner.nextLine().split(" ");

        int n = Integer.parseInt(nm[0]);

        int m = Integer.parseInt(nm[1]);

        int a[] = new int[m];
        int b[] = new int[m];
        int k[] = new int[m];

        for (int i = 0; i < m; i++) {
            nm = bufferedReader.readLine().split(" ");
            a[i] = Integer.parseInt(nm[0]);
            b[i] = Integer.parseInt(nm[1]);
            k[i] = Integer.parseInt(nm[2]);
        }

        bufferedReader.close();
        scanner.close();
        int result = solve(n, m, a, b, k);

        System.out.println(result);

    }
}
